# Práctica: JQuery, Ajax, DOM, JSON
## Descripción
Implementar una página web que muestre las películas listadas por la API proporcionada por el sitio [The Movie DB](https://www.themoviedb.org/)
## Descripción de la solución
La presentación de los resultados en la interfaz de búsqueda se realizó  a través de una lista, utilice Bootstrap para realizar la implementación, ya que me permitía obtener los resultados visuales que requería.  
La lista de resultados se llena a través de la manipulación del DOM, por medio de JQuery  se inserta contenido en la definición de la lista, esta se llena con el objeto JSON que devuelve la llamada a la API y se le da formato con Bootstrap. Además se implementó dos alertas, cuando no se encontró ninguna coincidencia con la búsqueda y otra para informar sobre un error; en las siguientes imágenes se puede observar la funcionalidad de la interfaz de búsqueda. 

**Presentación de la búsqueda**
![alt text](images/resultados.png)

**Alerta cuando no se encontró ninguna coincidencia**
![alt text](images/alertaInformacion.png)

**Alerta cuando hubo un error**
![alt text](images/alertaError.png)
## Autor
* Castelan Hernandez Mario
### Contacto
mario36010h@outlook.com