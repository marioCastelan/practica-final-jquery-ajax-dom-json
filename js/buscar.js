// Castelan Hernandez Mario
$(document).ready(function () {

	
	var busqueda = $("#busqueda");
	var miLista = $("#miLista");
	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);

		

	$.ajax({
		url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
		success: function(respuesta) {
			console.log(respuesta);
			$('#alerta').remove();
			if (respuesta.results.length !== 0) {
				console.log("Realizando busqueda");
				setTimeout(function () {
					$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)
	
					//Para cada elemento en la lista de resultados (para cada pelicula)
					$.each(respuesta.results, function(index, elemento) {
						//La función crearMovieCard regresa el HTML con la estructura de la pelicula
						cardHTML = crearMovieCard(elemento); 
						miLista.append(cardHTML);
					});
	
				}, 1000); //Retardo de funcion callback 
						  //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.
			}
			else {
				$('#loader').remove();
				$('#alerta').remove();
				console.log("Busqueda vacia");
				$("#buscar").append(
					'<div id="alerta" class="alert alert-info text-center p-5" role="alert">'
						+'No se encontró ninguna coincidencia'
					+'</div>'
				);
			}

		},
		error: function() {
			console.log("No se ha podido obtener la información");
			$('#loader').remove();
			$('#alerta').remove();
			$("#buscar").append(
				'<div id="alerta" class="alert alert-danger text-center p-5" role="alert">'
				+'No se ha podido obtener la información'
				+'</div>'			
			)
		},
		beforeSend: function() { 
			//ANTES de hacer la petición se muestra la imagen de cargando.
			console.log('CARGANDO');
			$('#alerta').remove();
			$('#miLista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
		},
	});	
});
});


function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
    //sabemos que el directorio donde se guardan las imágenes es: https://image.tmdb.org/t/p/w500/
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

	var cardHTML =
	'<li class="media shadow-sm p-3 mb-5 bg-white rounded ">'
		+'<img style="max-width:64px; class="mr-3" src="https://image.tmdb.org/t/p/w500' + movie.poster_path + '" alt="Poster de pelicula">'
		+'<div class="media-body pl-5">'
		+'<h5 class="mt-0 mb-1">'+movie.original_title+'</h5>'
		+movie.overview
		+'</div>'
	+'</li>'
	return cardHTML;

}